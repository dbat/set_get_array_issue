@tool
extends Node2D

## Bug Demo
##
## Press the tog property in the inspector to run the code.
## Afaict, the 'r.bugs' array should be set by 'set' but
## it is not. This used to work before Godot 4.3.dev1

@export var tog:bool:
	set(b):
		test()

## Works the same as an external .gd script file, so
## I put it in here for ease.
class Bugz extends Resource:
	@export var bugs : Array[Resource]


func test():
	var r = Bugz.new()
	var n = Resource.new() # some resource to append

	r.bugs.append(n)

	print("\nShould be [n]:", r.bugs, " OK")

	r.set("bugs",[])
	print("Should be []:", r.bugs, " NOPE")

	r.set("bugs",[n,n,n])
	print("Should be [n,n,n]:", r.bugs, " NOPE")
